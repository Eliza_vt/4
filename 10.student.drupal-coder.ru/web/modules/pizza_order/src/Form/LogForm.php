<?php

namespace Drupal\pizza_order\Form;

use Drupal\Component\Utility\EmailValidator;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class LogForm extends FormBase {

    /**
     * Returns a unique string identifying the form.
     *
     * The returned ID should be a unique string that can be a valid PHP function
     * name, since it's used in hook implementation names such as
     * hook_form_FORM_ID_alter().
     *
     * @return string
     *   The unique string identifying the form.
     */
    public function getFormId() {
        return 'pizza_order_form';
    }

    /**
     * Form constructor.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     *
     * @return array
     *   The form structure.
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $account = \Drupal::currentUser();
        if ($account->isAnonymous())
            $form['name'] = [
                '#placeholder' => 'Имя',
                '#type' => 'textfield',
                '#required' => TRUE,
            ];

        $config = \Drupal::config('pizza_order.settings');


        $form['Address'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Адрес'),
            '#required' => TRUE,
        ];

        $form['Tel'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Телефон'),
            '#required' => TRUE,
        ];


        // Add a submit button that handles the submission of the form.
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Оформить заказ'),
        ];

        return $form;


    }

    public function validateForm(array &$form, FormStateInterface $form_state) {
        parent::validateForm($form, $form_state);
        $email_validator = \Drupal::service('email.validator');
        assert($email_validator instanceof \Egulias\EmailValidator\EmailValidator);
        if (!($email_validator->isValid($form_state->getValue('email'))))
            if (\Drupal::currentUser()->isAnonymous() && !($email_validator->isValid($form_state->getValue('email'))))
                $form_state->setErrorByName('email', $this->t('This value is forbidden!'));
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {
        $account = \Drupal::currentUser();
        $flag = true;
        $info = $form_state->getUserInput();
        if ($account->isAuthenticated())
            $info = array_merge(array($account->getAccountName()), $info);
        if (empty($form_state->getErrors())) {
            \Drupal::logger('pizza_order')
                ->notice(serialize($info));
            \Drupal::messenger()->addMessage('Спасибо! Ваши данные записаны');
        } else {
            \Drupal::logger('pizza_order')
                ->error($form_state->getErrors());
        }
    }

}