<?php

namespace Drupal\write_log\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class ExampleSettingsForm extends ConfigFormBase {

    /**
     * Config settings.
     *
     * @var string
     */
    const SETTINGS = 'pizza_order.settings';

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'example_admin_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            static::SETTINGS,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config(static::SETTINGS);
        echo $config->get('Flag');
        $form['example_thing'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Анонимные пользователи могут указывать контактную информацию'),
            '#default_value' => $config->get('Flag'),
        ];

        $form['Area'] = [
            '#type' => 'radios',
            '#title' => $this->t('Район'),
            '#required' => TRUE,
            '#options' => array(
                0 => $this
                    ->t('ФМР'),
                1 => $this
                    ->t('Центральный'),
                2 => $this
                    ->t('Фестивальный'),
                3 => $this
                    ->t('Юбилейный'),
                4 => $this
                    ->t('Пашковский'),
                5 => $this
                    ->t('СЛавянский'),
                6 => $this
                    ->t('Черемушки'),
                7 => $this
                    ->t('Комсомольский'),
                8 => $this
                    ->t('Микрохирургии глаза'),
            ),
        ];

        $form['Pizza1'] = [
            '#type' => 'select',
            '#title' => $this->t('Маргарита'),
            '#required' => TRUE,
            '#options' => [
                '1' => $this
                    ->t('0'),
                '2' => $this
                    ->t('1'),
                '3' => $this
                    ->t('2'),
                '4' => $this
                    ->t('3'),
                '5' => $this
                    ->t('4'),
                '6' => $this
                    ->t('5'),
                '7' => $this
                    ->t('6'),
                '8' => $this
                    ->t('7'),
                '9' => $this
                    ->t('8'),
                '10' => $this
                    ->t('9'),
                '11' => $this
                    ->t('10'),
            ],
        ];

        $form['Pizza2'] = [
            '#type' => 'select',
            '#title' => $this->t('Ассорти'),
            '#required' => TRUE,
            '#options' => [
                '1' => $this
                    ->t('0'),
                '2' => $this
                    ->t('1'),
                '3' => $this
                    ->t('2'),
                '4' => $this
                    ->t('3'),
                '5' => $this
                    ->t('4'),
                '6' => $this
                    ->t('5'),
                '7' => $this
                    ->t('6'),
                '8' => $this
                    ->t('7'),
                '9' => $this
                    ->t('8'),
                '10' => $this
                    ->t('9'),
                '11' => $this
                    ->t('10'),
            ],
        ];

        $form['Pizza3'] = [
            '#type' => 'select',
            '#title' => $this->t('Мясная'),
            '#required' => TRUE,
            '#options' => [
                '1' => $this
                    ->t('0'),
                '2' => $this
                    ->t('1'),
                '3' => $this
                    ->t('2'),
                '4' => $this
                    ->t('3'),
                '5' => $this
                    ->t('4'),
                '6' => $this
                    ->t('5'),
                '7' => $this
                    ->t('6'),
                '8' => $this
                    ->t('7'),
                '9' => $this
                    ->t('8'),
                '10' => $this
                    ->t('9'),
                '11' => $this
                    ->t('10'),
            ],
        ];
        $form['Pizza4'] = [
            '#type' => 'select',
            '#title' => $this->t('Четыре сыра'),
            '#required' => TRUE,
            '#options' => [
                '1' => $this
                    ->t('0'),
                '2' => $this
                    ->t('1'),
                '3' => $this
                    ->t('2'),
                '4' => $this
                    ->t('3'),
                '5' => $this
                    ->t('4'),
                '6' => $this
                    ->t('5'),
                '7' => $this
                    ->t('6'),
                '8' => $this
                    ->t('7'),
                '9' => $this
                    ->t('8'),
                '10' => $this
                    ->t('9'),
                '11' => $this
                    ->t('10'),
            ],
        ];



        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        // Retrieve the configuration.
        $this->configFactory->getEditable(static::SETTINGS)
            // Set the submitted configuration setting.
            ->set('Flag', $form_state->getValue('example_thing'))
            // You can set multiple configurations at once by making
            // multiple calls to set().
//      ->set('other_things', $form_state->getValue('other_things'))
            ->save();

        parent::submitForm($form, $form_state);
    }

}