<?php

namespace Drupal\entity_creator\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines the dossier entity
 *
 * @ingroup dossier
 *
 * * @ContentEntityType(
 *   id = "dossier",
 *   label = @Translation("dossier"),
 *   base_table = "dossier",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 * )
 */
class Dossier extends ContentEntityBase implements ContentEntityInterface{

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Dossier entity.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Dossier entity.'))
      ->setReadOnly(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      -> setLabel(t('Name'))
      -> setDescription('Name of entity')
      -> setRequired('true');

    $fields['position'] = BaseFieldDefinition::create('string')
      -> setLabel(t('Position'))
      -> setDescription('Position of entity');

    $fields['bio'] = BaseFieldDefinition::create('string')
      -> setLabel(t('Bio'))
      -> setDescription('Bio of entity');

    return $fields;

  }

}
