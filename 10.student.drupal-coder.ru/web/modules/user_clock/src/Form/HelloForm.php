<?php

namespace Drupal\user_clock\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

class HelloForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'ex81_ajax_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
//    $account = \Drupal::currentUser();
//    if ($account->id() == $user || $account->id() == 1) {
      $form['massage'] = [
        '#type' => 'markup',
        '#markup' => '<div class="result_message">' . t(format_date(time(), 'custom', 'l j F Y H:i:s')) . '</div>',
      ];

      $form['actions'] = [
        '#type' => 'button',
        '#value' => $this->t('Обновить'),
        '#ajax' => [
          'callback' => '::setMessage'
        ]
      ];

      return $form;

//    }
//    else {
//      throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
//    }
  }

  public function setMessage(array &$form, FormStateInterface $form_state) {

    // Display the results.

    // Call the Static Service Container wrapper
    // We should inject the messenger service, but its beyond the scope of this example.
    $response = new AjaxResponse();

    $response->addCommand(
      new HtmlCommand(
        '.result_message',
        '<div class="my_top_message">' . $this->t(format_date(time(), 'custom', 'l j F Y H:i:s'))
      )
    );

    return $response;


  }

  public function submitForm(array &$form, FormStateInterface $form_state) {


  }

}