<?php

namespace Drupal\goodblock\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "hello_block",
 *   admin_label = @Translation("Hello block"),
 *   category = @Translation("goodblock"),
 * )
 */
class GoodBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
      $user = \Drupal::currentUser()->id();
      $d = getdate(); // использовано текущее время
      return [
      '#markup' => $this->t("Hello, " . $user. "Сегодня: " . $d[mday]. "." . $d[mon].".".$d[year] .
          " Время: ".$d[hours].":".$d[minutes].":".$d[seconds].
          " День недели: ".$d[weekday]." Месяц: ".$d[month]) ,
        '#cache' => ['contexts' => ['user']],
    ];

  }

    function hook_cron() {

        // Short-running operation example, not using a queue:
        // Delete all expired records since the last cron run.
        $expires = \Drupal::state()
            ->get('GoodBlock.last_check', 0);
        \Drupal::database()
            ->delete('GoodBlock_table')
            ->condition('expires', $expires, '>=')
            ->execute();
        \Drupal::state()
            ->set('GoodBlock.last_check', REQUEST_TIME);

        // Long-running operation example, leveraging a queue:
        // Queue news feeds for updates once their refresh interval has elapsed.
        $queue = \Drupal::queue('aggregator_feeds');
        $ids = \Drupal::entityTypeManager()
            ->getStorage('aggregator_feed')
            ->getFeedIdsToRefresh();
        foreach (Feed::loadMultiple($ids) as $feed) {
            if ($queue
                ->createItem($feed)) {

                // Add timestamp to avoid queueing item more than once.
                $feed
                    ->setQueuedTime(REQUEST_TIME);
                $feed
                    ->save();
            }
        }
        $ids = \Drupal::entityQuery('aggregator_feed')
            ->condition('queued', REQUEST_TIME - 3600 * 6, '<')
            ->execute();
        if ($ids) {
            $feeds = Feed::loadMultiple($ids);
            foreach ($feeds as $feed) {
                $feed
                    ->setQueuedTime(0);
                $feed
                    ->save();
            }
        }
    }


    public static function currentUser() {
        return static::getContainer()
            ->get('current_user');
    }



}